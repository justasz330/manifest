Kuriai kategorijai priskirtumėte duomenų rinkinį:

- Žemės ūkis, žuvininkystė, miškininkystė ir maitas
- Energetika
- Regionai ir miestai
- Transportas
- Ekonomika ir finansai
- Tarptautiniai klausimai
- Švietimas, kultūra ir sportas
- Teisingumas, teisinė sistema ir visuomenės sauga
- Aplinka
- Valstybė ir viešasis sektorius
- Gyventojų skaičius ir visuomenė
- Mokslas ir technologijos
- Sveikata

Nurodykite, kokio periodo duomenų reikia (formatu MMMM/mm/dd):

Nuo:

Iki:

Naudojimo paskirtis:

- Moksliniams tyrimams, studijoms
- Naujų paslaugų, produktų sukūrimui
- Pilietinės visuomenės, demokratinių procesų skatinimui
- Visuomenės informavimui
- Socialinių ar aplinkosauginių problemų sprendimui

Reikalingas duomenų atnaujinimo dažnis:

- Neaktualu
- Kartą per metus
- Kartą per mėnesį
- Kartą per savaitę
- Kartą per dieną
- Atsiradus naujiems duomenims

Pageidaujamas duomenų formatas:

- CSV
- JSON
- RDF
- XML

Duomenų valdytoja (nurodykite, kokia institucija turėtų valdyti informacinę
sistemą, kurioje kaupiami prašomi duomenys):


Pageidaujamų duomenų laukų sąrašas:

| Duomens pavadinimas                  | Duomens pavadinimas standartiniame žodyne | Duomenų tipas          |
| ------------------------------------ | ----------------------------------------- | ---------------------- |
|                                      |                                           |                        |


Papildomi komentarai, pastabos:



/label ~Poreikis 
/assign @atviriduomenys 
